﻿using System;

namespace Lab3
{
    class Kube : Line
    {
        public int C { get; set; }

        public Kube(int a, int b, int c, int x) : base(a, b, x)
        {
            C = c;
        }

        public override int Calc()
        {
            return A * X * X + B * X + C;
        }

        public override void Print()
        {
            Console.WriteLine("\nKub\nА равно: {0} \nВ равно: {1} \nC равно: {2} \nX равно: {3}", A, B, C, X);
            Console.WriteLine("Y равно: {0}", Calc());
        }
    }
}
