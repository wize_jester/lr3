﻿namespace Lab3
{
    abstract class Publisher
    {
        public string Name { get; set; }
        public string Author { get; set; }

        abstract public void Print();
    }
}
